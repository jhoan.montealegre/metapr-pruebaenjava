package com.employee.soap_service.services.impl;

import com.employee.soap_service.entities.EmployeeEntity;
import com.employee.soap_service.repositories.EmployeeEntityRepository;
import com.employee.soap_service.services.EmployeeEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("employeeService")
public class EmployeeEntityServiceImpl implements EmployeeEntityService {

    private EmployeeEntityRepository repository;

    public EmployeeEntityServiceImpl() {

    }

    @Autowired
    public EmployeeEntityServiceImpl(EmployeeEntityRepository repository) {
        this.repository = repository;
    }


    @Override
    public EmployeeEntity addEntity(EmployeeEntity entity) {
        try {
            return this.repository.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
