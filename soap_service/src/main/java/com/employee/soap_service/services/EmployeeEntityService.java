package com.employee.soap_service.services;

import com.employee.soap_service.entities.EmployeeEntity;

public interface EmployeeEntityService {

    public EmployeeEntity addEntity(EmployeeEntity entity);
}
