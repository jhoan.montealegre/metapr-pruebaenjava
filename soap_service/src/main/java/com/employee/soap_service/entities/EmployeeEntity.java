package com.employee.soap_service.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "employee")
public class EmployeeEntity {

    @Id
    @GeneratedValue
    private long   Id;
    private String names ;
    private String lastName ;
    private String documentType;
    private String documentNumber ;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date   birthday;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date   boindingJob;
    private String jobPosition;
    private Double payRate;

    public EmployeeEntity() {

    }

    public EmployeeEntity(long id, String names, String lastName, String documentType, String documentNumber, Date birthday, Date boindingJob, String jobPosition, Double payRate) {
        Id = id;
        this.names = names;
        this.lastName = lastName;
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.birthday = birthday;
        this.boindingJob = boindingJob;
        this.jobPosition = jobPosition;
        this.payRate = payRate;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String last_name) {
        this.lastName = last_name;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getBoindingJob() {
        return boindingJob;
    }

    public void setBoindingJob(Date boindingJob) {
        this.boindingJob = boindingJob;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public Double getPayRate() {
        return payRate;
    }

    public void setPayRate(Double payRate) {
        this.payRate = payRate;
    }

    @Override
    public String toString() {
        return "EmployeeEntity{" +
                "Id=" + Id +
                ", names='" + names + '\'' +
                ", last_name='" + lastName + '\'' +
                ", documentType='" + documentType + '\'' +
                ", documentoNumber='" + documentNumber + '\'' +
                ", birthday=" + birthday +
                ", boindingJob=" + boindingJob +
                ", jobPosition='" + jobPosition + '\'' +
                ", payRate=" + payRate +
                '}';
    }
}
