package com.employee.soap_service.repositories;

import com.employee.soap_service.entities.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface EmployeeEntityRepository extends JpaRepository<EmployeeEntity, Serializable> {
}
