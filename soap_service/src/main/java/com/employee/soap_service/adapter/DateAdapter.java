package com.employee.soap_service.adapter;

import javax.xml.bind.DatatypeConverter;
import java.util.Calendar;
import java.util.Date;

public class DateAdapter  {
    public static Date parseDate(String s) {
        if (s == null) {
            return null;
        }

        return DatatypeConverter.parseDate(s).getTime();
    }

    public static String printDate(Date dt) {
        if (dt == null) {
            return null;
        }

        Calendar c = Calendar.getInstance();
        c.setTime(dt);

        return DatatypeConverter.printDate(c);
    }
}