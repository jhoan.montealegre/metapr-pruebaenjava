package com.employee.soap_service.enpoints;

import com.employee.soap_service.entities.EmployeeEntity;
import com.employee.soap_service.services.EmployeeEntityService;


import employee.soap_service.employee_ws.AddEmployeeRequest;
import employee.soap_service.employee_ws.AddEmployeeResponse;
import employee.soap_service.employee_ws.EmployeeType;
import employee.soap_service.employee_ws.ServiceStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class EmployeeEndpoint {

    public static final String NAMESPACE_URI = "http://www.testDeveloper.com/employee-ws";



    @Autowired
    @Qualifier("employeeService")
    private EmployeeEntityService serviceEmployee;
/*

    public MovieEndpoint() {

    }

    @Autowired
    public MovieEndpoint(MovieEntityService service) {
        this.service = service;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMovieByIdRequest")
    @ResponsePayload
    public GetMovieByIdResponse getMovieById(@RequestPayload GetMovieByIdRequest request) {
        GetMovieByIdResponse response = new GetMovieByIdResponse();
        MovieEntity movieEntity = service.getEntityById(request.getMovieId());
        MovieType movieType = new MovieType();
        BeanUtils.copyProperties(movieEntity, movieType);
        response.setMovieType(movieType);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllMoviesRequest")
    @ResponsePayload
    public GetAllMoviesResponse getAllMovies(@RequestPayload GetAllMoviesRequest request) {
        GetAllMoviesResponse response = new GetAllMoviesResponse();
        List<MovieType> movieTypeList = new ArrayList<MovieType>();
        List<MovieEntity> movieEntityList = service.getAllEntities();
        for (MovieEntity entity : movieEntityList) {
            MovieType movieType = new MovieType();
            BeanUtils.copyProperties(entity, movieType);
            movieTypeList.add(movieType);
        }
        response.getMovieType().addAll(movieTypeList);

        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addMovieRequest")
    @ResponsePayload
    public AddMovieResponse addMovie(@RequestPayload AddMovieRequest request) {
        AddMovieResponse response = new AddMovieResponse();
        MovieType newMovieType = new MovieType();
        ServiceStatus serviceStatus = new ServiceStatus();

        MovieEntity newMovieEntity = new MovieEntity(request.getTitle(), request.getCategory());
        MovieEntity savedMovieEntity = service.addEntity(newMovieEntity);

        if (savedMovieEntity == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding Entity");
        } else {

            BeanUtils.copyProperties(savedMovieEntity, newMovieType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }

        response.setMovieType(newMovieType);
        response.setServiceStatus(serviceStatus);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateMovieRequest")
    @ResponsePayload
    public UpdateMovieResponse updateMovie(@RequestPayload UpdateMovieRequest request) {
        UpdateMovieResponse response = new UpdateMovieResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        // 1. Find if movie available
        MovieEntity movieFromDB = service.getEntityByTitle(request.getTitle());

        if(movieFromDB == null) {
            serviceStatus.setStatusCode("NOT FOUND");
            serviceStatus.setMessage("Movie = " + request.getTitle() + " not found");
        }else {

            // 2. Get updated movie information from the request
            movieFromDB.setTitle(request.getTitle());
            movieFromDB.setCategory(request.getCategory());
            // 3. update the movie in database

            boolean flag = service.updateEntity(movieFromDB);

            if(flag == false) {
                serviceStatus.setStatusCode("CONFLICT");
                serviceStatus.setMessage("Exception while updating Entity=" + request.getTitle());;
            }else {
                serviceStatus.setStatusCode("SUCCESS");
                serviceStatus.setMessage("Content updated Successfully");
            }


        }

        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteMovieRequest")
    @ResponsePayload
    public DeleteMovieResponse deleteMovie(@RequestPayload DeleteMovieRequest request) {
        DeleteMovieResponse response = new DeleteMovieResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        boolean flag = service.deleteEntityById(request.getMovieId());

        if (flag == false) {
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Exception while deletint Entity id=" + request.getMovieId());
        } else {
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Deleted Successfully");
        }

        response.setServiceStatus(serviceStatus);
        return response;
    }
*/
    private Log LOG = LogFactory.getLog(EmployeeEndpoint.class);

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addEmployeeRequest")
    @ResponsePayload
    public AddEmployeeResponse addEmployee(@RequestPayload AddEmployeeRequest request) {

        AddEmployeeResponse response = new AddEmployeeResponse();
        EmployeeType newEmployeeType = new EmployeeType();
        ServiceStatus serviceStatus = new ServiceStatus();
        EmployeeEntity newEmployeeEntity = new EmployeeEntity();
        BeanUtils.copyProperties(request,newEmployeeEntity);

       // EmployeeEntity newEmployeeEntity = new MovieEntity(request.getTitle(), request.getCategory());
        EmployeeEntity savedEmployeeEntity = serviceEmployee.addEntity(newEmployeeEntity);
        if (savedEmployeeEntity == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding Entity");
        } else {
            BeanUtils.copyProperties(savedEmployeeEntity, newEmployeeType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }
        response.setEmployeeType(newEmployeeType);
        response.setServiceStatus(serviceStatus);
        return response;

    }

}
